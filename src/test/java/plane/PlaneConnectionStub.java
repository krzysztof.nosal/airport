package plane;

import common.TcpTools;
import json.CommandFromAirTraffic;

import java.io.DataOutputStream;
import java.util.concurrent.ArrayBlockingQueue;

public class PlaneConnectionStub implements PlaneConnectionInterface{
    private final ArrayBlockingQueue<CommandFromAirTraffic> trafficControlInputBuffer;
    private boolean messageSent;
    private String lastSentMessage;

    public PlaneConnectionStub(ArrayBlockingQueue<CommandFromAirTraffic> trafficControlInputBuffer) {
        this.trafficControlInputBuffer=trafficControlInputBuffer;
        trafficControlInputBuffer = new ArrayBlockingQueue<>(100, true);

    }

    public boolean hasMessageBeenSent(){
        return messageSent;
    }

    public String getLastSentMessage() {
        messageSent=false;
        return lastSentMessage;

    }

    @Override
    public ArrayBlockingQueue<CommandFromAirTraffic> getRecentCommands() {
        if (trafficControlInputBuffer.peek() != null) {
            return trafficControlInputBuffer;
        } else {
            return null;
        }
    }

    @Override
    public CommandFromAirTraffic getLastCommand() {
        return trafficControlInputBuffer.poll();
    }

    @Override
    public boolean areCommandsInBuffer() {
        return trafficControlInputBuffer.peek() != null;
    }

    @Override
    public void closeConnection() {
        return;
    }

    @Override
    public void sendMessage(String message) {
        System.out.println("message sent to traffic control: "+message);
        lastSentMessage=message;
        messageSent=true;
    }
}
