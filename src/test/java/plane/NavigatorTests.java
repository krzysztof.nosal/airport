package plane;

import common.Position;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

public class NavigatorTests {
    @Test
    @DisplayName("")
    public void findingNextPointOnCircle(){
        Position startPosition = new Position(0,1000,1000);
        Position nextPosition = Navigator.findNextPositionOnCircle(startPosition,new Position(0,0,1000),800,1);
        System.out.println(nextPosition);
    }
}
