package air_traffic_control;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.util.concurrent.atomic.AtomicLong;

public class LandingStageTest {

    @Test
    @DisplayName("")
    public void SendPlaneToCircleTest() {
        LandingStage landingStage = new LandingStage(2000, 10000, 4000, 5000, 5000, 500);
        AtomicLong timestamp = new AtomicLong(0);
        Assert.assertTrue(true);
        PlaneInQueue planToBeSent = new PlaneInQueue(1234, 0);

        int targetHeight = landingStage.findClosestFreeCircle(4000);
        planToBeSent.setTargetHeight(targetHeight);
        landingStage.sendPlaneToCircle(planToBeSent, 4000);
        Assert.assertEquals(planToBeSent.planeNo, landingStage.getFirstPlaneFromIntermediateStage().planeNo);
        Assert.assertEquals(planToBeSent.targetHeight, landingStage.getFirstPlaneFromIntermediateStage().targetHeight);
        timestamp.set(10);
        Assert.assertEquals(0, landingStage.getListOfAvailableCircles().get(targetHeight).getPlanesCount());

        Assert.assertFalse(landingStage.checkIfPlaneOnCircleAnProceed(timestamp, 20));
        timestamp.set(50);
        Assert.assertTrue(landingStage.checkIfPlaneOnCircleAnProceed(timestamp, 45));
        Assert.assertNull(landingStage.getFirstPlaneFromIntermediateStage());
        Assert.assertEquals(planToBeSent.planeNo, landingStage.getFirstPlaneFromLandingStage().planeNo);
        Assert.assertTrue(landingStage.arePlanesOnLandingStage());
        Assert.assertEquals(1, landingStage.getListOfAvailableCircles().get(targetHeight).getPlanesCount());
        landingStage.removePlaneFromStage(planToBeSent);
        Assert.assertEquals(0, landingStage.getListOfAvailableCircles().get(targetHeight).getPlanesCount());
        Assert.assertFalse(landingStage.arePlanesOnLandingStage());
    }


    @Test
    @DisplayName("")
    public void GetFreeCircleTest() {

        LandingStage landingStage = new LandingStage(2000, 10000, 4000, 5000, 5000, 500);

        Assert.assertEquals(4000, landingStage.findClosestFreeCircle(4000));
        landingStage.getListOfAvailableCircles().get(4000).setReservation(true);

        Assert.assertEquals(3500, landingStage.findClosestFreeCircle(4000));

        landingStage.getListOfAvailableCircles().get(2000).addPlane(1234);
        landingStage.getListOfAvailableCircles().get(2000).addPlane(134);

        Assert.assertEquals(2500, landingStage.findClosestFreeCircle(2000));
        landingStage.getListOfAvailableCircles().get(2500).setReservation(true);
        Assert.assertEquals(3000, landingStage.findClosestFreeCircle(2500));
        landingStage.getListOfAvailableCircles().get(3000).setReservation(true);
        landingStage.getListOfAvailableCircles().get(3500).setReservation(true);
        landingStage.getListOfAvailableCircles().get(4000).setReservation(true);
        landingStage.getListOfAvailableCircles().get(4500).setReservation(true);
        landingStage.getListOfAvailableCircles().get(5000).setReservation(true);
        landingStage.getListOfAvailableCircles().get(5500).setReservation(true);
        landingStage.getListOfAvailableCircles().get(6000).setReservation(true);
        landingStage.getListOfAvailableCircles().get(6500).setReservation(true);
        landingStage.getListOfAvailableCircles().get(7000).setReservation(true);
        landingStage.getListOfAvailableCircles().get(7500).setReservation(true);
        landingStage.getListOfAvailableCircles().get(8000).setReservation(true);
        landingStage.getListOfAvailableCircles().get(8500).setReservation(true);
        landingStage.getListOfAvailableCircles().get(9000).setReservation(true);
        landingStage.getListOfAvailableCircles().get(9500).setReservation(true);
        landingStage.getListOfAvailableCircles().get(10000).setReservation(true);
        Assert.assertEquals(0, landingStage.findClosestFreeCircle(3500));
        landingStage.getListOfAvailableCircles().get(2500).setReservation(false);
        Assert.assertEquals(2500, landingStage.findClosestFreeCircle(3500));
        Assert.assertEquals(0, landingStage.findClosestFreeCircle(1000));

    }

    @Test
    @DisplayName("")
    public void IsCrossingBlockedAirZoneTest() {
        LandingStage landingStage = new LandingStage(2000, 10000, 4000, 5000, 5000, 500);
        landingStage.getBlockedAirZones().put(1234, new AirZone(3000, 4000));
        try {
            var method = landingStage.getClass().getDeclaredMethod("isCrossingBlockedAirZone", int.class, int.class);
            method.setAccessible(true);
            Assert.assertFalse((boolean) method.invoke(landingStage, 2000, 2500));
            Assert.assertFalse((boolean) method.invoke(landingStage, 8000, 5000));
            Assert.assertTrue((boolean) method.invoke(landingStage, 5000, 3500));
            Assert.assertTrue((boolean) method.invoke(landingStage, 5000, 2000));
            Assert.assertTrue((boolean) method.invoke(landingStage, 2000, 3500));
            Assert.assertTrue((boolean) method.invoke(landingStage, 2000, 5000));
            Assert.assertTrue((boolean) method.invoke(landingStage, 3500, 5000));
            Assert.assertTrue((boolean) method.invoke(landingStage, 3500, 2000));
            Assert.assertTrue((boolean) method.invoke(landingStage, 3200, 3700));
            Assert.assertTrue((boolean) method.invoke(landingStage, 3700, 3200));
        } catch (Exception e) {
            Assert.fail();
        }
    }
}
