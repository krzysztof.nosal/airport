package air_traffic_control;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import sql_connection_pool.ConnectionPool;
import sql_connection_pool.PoolCleaner;

import java.util.concurrent.atomic.AtomicLong;

public class AirZoneTest {
    @Test
    @DisplayName("")
    public void AirZoneConstructorTest(){
        AirZone airZone1=new AirZone(1234, 5453);
        Assert.assertTrue(airZone1.getMaxHeight()==5453);
        AirZone airZone2=new AirZone(6532, 123);
        Assert.assertTrue(airZone2.getMaxHeight()==6532);
    }
}
