package main;

import air_traffic_control.*;
import common.SqlTools;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import plane.BatchPlaneEvaluation;
import plane.Plane;
import simulation.RandomPlaneGenerator;
import simulation.TimestampIncrementer;
import sql_connection_pool.ConnectionPool;
import sql_connection_pool.PoolCleaner;
import sql_connection_pool.SqlConn;

import java.sql.Statement;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class AirportSimulation {
    @Test
    @DisplayName("")
    public void trafficControlSimulation() {
        //incrementing internal timestamp of simulation
        AtomicLong timestamp = new AtomicLong(1);
        TimestampIncrementer timestampIncrementer = new TimestampIncrementer(timestamp);
        timestampIncrementer.start();

        //creating sql connection pool for planes
        ConnectionPool sqlConnectionPool = new ConnectionPool();
        sqlConnectionPool.start();
        PoolCleaner poolCleaner = new PoolCleaner(sqlConnectionPool);
        poolCleaner.start();
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        SqlTools.clearDb(sqlConnectionPool);
        //creating tcp listener to accept connections from planes
        ConcurrentHashMap<Integer, PlaneManager> planeManagerConcurrentHashMap = new ConcurrentHashMap<>();

        Radar radar = new Radar(sqlConnectionPool.getSqlCon(), timestamp);
        radar.start();

        AirMap airMap = AirMapFactory.prepareDefaultAirmap(radar);

        Listener tcpListener = new Listener(planeManagerConcurrentHashMap, 1234, airMap);
        tcpListener.start();

        //generating planes in random places on border of air area
        Vector<Plane> planesVector = new Vector<>();

        SqlConn connection = SqlTools.getConnectionFromPool(sqlConnectionPool);
        Statement batchStatement = null;
        try {
            batchStatement = connection.getCon().createStatement();
        } catch (Exception e) {
            e.printStackTrace();
        }

        BatchPlaneEvaluation batchPlaneEvaluation = new BatchPlaneEvaluation(planesVector, batchStatement);
        batchPlaneEvaluation.start();

        RandomPlaneGenerator randomPlaneGenerator = new RandomPlaneGenerator(planesVector, timestamp, sqlConnectionPool, batchStatement);
        randomPlaneGenerator.start();


        TrafficControl trafficControl = new TrafficControl(planeManagerConcurrentHashMap, radar, timestamp, airMap);
        trafficControl.start();
        while (planesVector.isEmpty() || planesVector.get(planesVector.size() - 1).getPlaneNo() < 150) {
            try {
                Thread.sleep(50000);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        Assert.assertTrue(true);
    }
}
