package main;

import monitoring.PlanePathFileGenerator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import sql_connection_pool.ConnectionPool;
import sql_connection_pool.PoolCleaner;
import sql_connection_pool.SqlConn;

public class PlanePathFileGeneratorTest {
    @Test
    @DisplayName("")
    public void runTestPlane(){
        ConnectionPool connectionPool=new ConnectionPool();
        try {
            Thread.sleep(1000);
        }catch (Exception e){
            e.printStackTrace();
        }
        PoolCleaner poolCleaner = new PoolCleaner(connectionPool);
        poolCleaner.start();

        SqlConn connection2=null;
        while (connection2==null) {
            connection2 = connectionPool.getSqlCon();
        }
        PlanePathFileGenerator planePathFileGenerator =new PlanePathFileGenerator(connection2,"D:\\__JAVA\\airport\\src\\main\\java");
        planePathFileGenerator.getPathAndSaveToFile(1234);
        System.out.println("made file");
        try {
            Thread.sleep(500000);
        }catch (Exception e){
            e.printStackTrace();
        }
        Assert.assertTrue(true);
    }

}
