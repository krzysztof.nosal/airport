package monitoring;

import sql_connection_pool.SqlConn;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class PlanePathFileGenerator {
    private final SqlConn sqlConnection;
    private final String directory;

    public PlanePathFileGenerator(SqlConn sqlConnection, String directory) {
        this.sqlConnection = sqlConnection;
        this.directory = directory;
    }

    public void getPathAndSaveToFile(int planeNo) {
        String query = "SELECT id, plane_no, x_position, y_position, z_position, flag, timestamp FROM plane_monitoring WHERE plane_no=" + planeNo + " ORDER BY timestamp;";
        String path = directory + "\\path" + planeNo + ".csv";
        try (var stmt = sqlConnection.getCon().createStatement()) {
            var resultSet = stmt.executeQuery(query);
            var stringBuilder = new StringBuilder();
            while (resultSet.next()) {
                stringBuilder.append(resultSet.getFloat("x_position")).append(";").append(resultSet.getFloat("y_position")).append(";").append(resultSet.getFloat("z_position")).append("\n");
            }
            saveFile(path, stringBuilder.toString());
        } catch (Exception e) {
            System.out.println("Error while getting data from db to create path file");
            e.printStackTrace();
        }

    }

    private void saveFile(String path, String content) {
        try {
            Files.writeString(Paths.get(path), content, StandardOpenOption.CREATE_NEW);
        } catch (Exception e) {
            System.out.println("error while writing path file");
            e.printStackTrace();
        }
    }

}
