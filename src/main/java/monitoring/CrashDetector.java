package monitoring;

import common.Position;
import plane.Navigator;
import sql_connection_pool.SqlConn;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class CrashDetector {
    SqlConn sqlConnection;

    public CrashDetector(SqlConn sqlConnection) {
        this.sqlConnection = sqlConnection;
    }

    private Map<Integer, String> getPlaneNumbers() {
        var query = "SELECT plane_no from plane_monitoring ORDER BY timestamp;";
        Map<Integer, String> planeMap = new LinkedHashMap<>();
        try (var stmt = sqlConnection.getCon().createStatement()) {
            var resultSet = stmt.executeQuery(query);
            while (resultSet.next()) {
                planeMap.put(resultSet.getInt("plane_no"), "ok");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return planeMap;
    }

    public boolean checkIfThereWasCrash() {
        Map<Integer, String> planeMap = getPlaneNumbers();
        Set<Integer> keys = planeMap.keySet();

        for (Integer key : keys) {
            try (var stmt1 = sqlConnection.getCon().createStatement();
                 var stmt2 = sqlConnection.getCon().createStatement()) {
                String queryForAllPlacesWherePlaneWas = "SELECT x_position, y_position, z_position, flag, timestamp FROM plane_monitoring WHERE plane_no=" + key + " AND flag<100 ORDER BY timestamp;";
                var resultSet = stmt1.executeQuery(queryForAllPlacesWherePlaneWas);
                while (resultSet.next()) {
                    var myPosition = new Position(resultSet.getFloat("x_position"), resultSet.getFloat("y_position"), resultSet.getFloat("z_position"));
                    String queryForAllOtherPlanesInSameMoment = "SELECT x_position, y_position, z_position, flag, plane_no FROM plane_monitoring WHERE timestamp=" + resultSet.getInt("timestamp") + " AND flag<100 ;";
                    var proximityCounter = 0;
                    var resultSet2 = stmt2.executeQuery(queryForAllOtherPlanesInSameMoment);
                    while (resultSet2.next()) {
                        var newPosition = new Position(resultSet2.getFloat("x_position"), resultSet2.getFloat("y_position"), resultSet2.getFloat("z_position"));
                        if (Navigator.distanceBetweenPositions(myPosition, newPosition) < 30) {
                            proximityCounter++;
                        }
                    }
                    if (proximityCounter > 1) {
                        System.out.println("plane no: " + key);
                        return true;
                    }
                }
                return false;

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return false;
    }
}
