package common;

import plane.Plane;
import sql_connection_pool.ConnectionPool;
import sql_connection_pool.SqlConn;

import java.util.concurrent.atomic.AtomicLong;

public class SqlTools {
    private SqlTools() {
    }

    public static SqlConn getConnectionFromPool(ConnectionPool connectionPool) {
        SqlConn sqlConn = null;
        while (sqlConn == null) {
            sqlConn = connectionPool.getSqlCon();
        }
        return sqlConn;
    }

//    public static Statement createStatementFromConnection(SqlConn conn) {
//        Statement statement = null;
//        while (statement == null) {
//            try {
//                statement = conn.getCon().createStatement();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//        }
//        return statement;
//    }

    public static String createSavePositionToDbStatement(Plane plane, AtomicLong timestamp) {
        return "INSERT INTO plane_monitoring(plane_no, x_position, y_position,z_position,flag,timestamp) VALUE (" + plane.getPlaneConfiguration().getPlaneNo() + "," + plane.getPosition().getX() + "," + plane.getPosition().getY() + "," + plane.getPosition().getZ() + "," + plane.getPlaneFlag() + "," + timestamp.get() + ");";
    }

    public static void clearDb(ConnectionPool sqlConnectionPool) {
        SqlConn connection = SqlTools.getConnectionFromPool(sqlConnectionPool);
        connection.executeUpdate("DELETE FROM plane_monitoring;");

        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        connection.setFree();
    }


}
