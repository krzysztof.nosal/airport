package common;

import plane.ClientConnectionHandler;
import plane.ClientConnectionHandlerInterface;

import java.util.concurrent.TimeUnit;

public class TcpTools {
    private TcpTools(){}

    public static ClientConnectionHandlerInterface getTcpConnectionHandler(int port, String address){
        var connectionHandler = new ClientConnectionHandler();
        while (!connectionHandler.connect(port, address)) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return connectionHandler;
    }
}
