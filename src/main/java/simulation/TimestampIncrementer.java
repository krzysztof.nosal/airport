package simulation;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

public class TimestampIncrementer extends Thread {
    private final AtomicLong timestamp;
    private final AtomicBoolean isRunning;
    public TimestampIncrementer(AtomicLong timestamp) {
        this.timestamp = timestamp;
        this.isRunning=new AtomicBoolean(true);
    }

    public void abort(){
        isRunning.set(false);
    }

    @Override
    public void run() {
        while (isRunning.get()){
            timestamp.getAndAdd(1);
            try{
                Thread.sleep(1000);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
