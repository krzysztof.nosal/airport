package simulation;

import common.Position;
import common.SqlTools;
import monitoring.PlanePathFileGenerator;
import plane.*;
import sql_connection_pool.ConnectionPool;
import sql_connection_pool.SqlConn;

import java.sql.Statement;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

public class RandomPlaneGenerator extends Thread {
    private final Vector<Plane> planesVector;
    private final AtomicBoolean running;
    private final AtomicLong timestamp;
    private final ConnectionPool connectionPool;
    private final Statement batchStatement;
    private int planeNo = 1;

    public RandomPlaneGenerator(Vector<Plane> planesVector, AtomicLong timestamp, ConnectionPool connectionPool, Statement batchStatement) {
        this.batchStatement = batchStatement;
        this.timestamp = timestamp;
        this.connectionPool = connectionPool;
        this.planesVector = planesVector;
        this.running = new AtomicBoolean(true);
    }

    private void cleanPlanesVector() {
        for (var i = 0; i < planesVector.size(); i++) {
            if (planesVector.get(i).getPlaneFlag() == PlaneFlag.LANDED) {
                planesVector.remove(i);
                break;
            }
        }
    }

    public void stopGenerator() {
        running.set(false);
    }

    @Override
    public void run() {
        SqlConn sqlCon = SqlTools.getConnectionFromPool(connectionPool);
        var planePathFileGenerator = new PlanePathFileGenerator(sqlCon, "D:\\__JAVA\\airport\\src\\main\\java");
        var random = new Random();
        while (running.get()) {
            var side = 1;
            if (random.ints(1, 5).findFirst().isPresent()) {
                side = random.ints(1, 5).findFirst().getAsInt();
            }
            var height = 3000;
            if (random.ints(2000, 5001).findFirst().isPresent()) {
                height = random.ints(2000, 5001).findFirst().getAsInt();
            }
            var position = 5000;
            if (random.ints(0, 10001).findFirst().isPresent()) {
                position = random.ints(0, 10001).findFirst().getAsInt();
            }
            var startPosition = new Position(0, 0, height);
            switch (side) {
                case 1:
                    startPosition.setX(0);
                    startPosition.setY(position);
                    break;
                case 2:
                    startPosition.setX(10000);
                    startPosition.setY(position);
                    break;
                case 3:
                    startPosition.setX(position);
                    startPosition.setY(0);
                    break;
                case 4:
                    startPosition.setX(position);
                    startPosition.setY(10000);
                    break;
                default:
                    break;
            }

            System.out.println("Generated planeNo: " + planeNo + " timestamp: " + timestamp.get());
            var planeConnection = new PlaneConnection(1234,"localhost");
            var planeSqlConnection = new PlaneSqlConnection(batchStatement);
            var plane = new Plane(new PlaneConfiguration(planeNo,700,startPosition ),planeSqlConnection, timestamp, planeConnection, planePathFileGenerator);
            planeNo++;
            planesVector.add(plane);
            cleanPlanesVector();
            var pause = random.ints(100000, 400000).findFirst().getAsInt();
            try {
                Thread.sleep(pause);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }
}
