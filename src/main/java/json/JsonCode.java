package json;

import com.google.gson.Gson;

public class JsonCode {
    private JsonCode(){}

    public static String codingCommandFromAirTraffic(CommandFromAirTraffic commandFromAirTraffic){
        var gson =new Gson();
        return gson.toJson(commandFromAirTraffic);
    }

    public static String codingResponseFromPlane(ResponseFromPlane responseFromPlane){
        var gson = new Gson();
        return gson.toJson(responseFromPlane);
    }
}
