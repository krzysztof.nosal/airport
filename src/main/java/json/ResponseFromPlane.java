package json;

import common.Position;

public class ResponseFromPlane {
    int planeNo;
    Position position;
    ResponseMessage responseMessage;

    public ResponseFromPlane(int planeNo, Position position, ResponseMessage responseMessage) {
        this.planeNo = planeNo;
        this.position = position;
        this.responseMessage = responseMessage;
    }

    public int getPlaneNo() {
        return planeNo;
    }

    public Position getPosition() {
        return position;
    }

    public ResponseMessage getResponseMessage() {
        return responseMessage;
    }
}
