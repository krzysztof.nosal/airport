package json;

import common.Position;

public class CommandFromAirTraffic {
    private CommandType commandType;
    private Position targetPosition;
    private Position circlingCenter;
    private int circlingRadius;
    private Runway runwayDefinition;
    private boolean confirmation;

    public CommandFromAirTraffic(Position targetPosition) {
        this.commandType = CommandType.GO_TO_POINT;
        this.targetPosition = targetPosition;
        this.circlingCenter = null;
        this.runwayDefinition = null;
        this.confirmation = false;
    }

    public CommandType getCommandType() {
        return commandType;
    }

    public void setCommandType(CommandType commandType) {
        this.commandType = commandType;
    }

    public Position getTargetPosition() {
        return targetPosition;
    }

    public void setTargetPosition(Position targetPosition) {
        this.targetPosition = targetPosition;
    }

    public Position getCirclingCenter() {
        return circlingCenter;
    }

    public void setCirclingCenter(Position circlingCenter) {
        this.circlingCenter = circlingCenter;
    }

    public int getCirclingRadius() {
        return circlingRadius;
    }

    public void setCirclingRadius(int circlingRadius) {
        this.circlingRadius = circlingRadius;
    }

    public Runway getRunwayDefinition() {
        return runwayDefinition;
    }

    public void setRunwayDefinition(Runway runwayDefinition) {
        this.runwayDefinition = runwayDefinition;
    }
}
