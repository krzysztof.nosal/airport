package json;

import common.Position;
import java.util.List;

public class Runway {
    private final List<Position> positionsArray;
    public Runway(List<Position> positionsArray) {
        this.positionsArray = positionsArray;
    }
    public List<Position> getPositionsArray() {
        return positionsArray;
    }

}
