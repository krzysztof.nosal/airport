package json;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class JsonDecode {

    private JsonDecode(){}

    public static CommandFromAirTraffic commandFromAirTraffic(String rawCommand){
        var gson = new Gson();
        CommandFromAirTraffic commandFromAirTraffic;
        var commandType = new TypeToken<CommandFromAirTraffic>(){}.getType();
        commandFromAirTraffic=gson.fromJson(rawCommand,commandType);
        return commandFromAirTraffic;
    }

    public static ResponseFromPlane responseFromPlane(String rawCommand){
        var gson = new Gson();
        ResponseFromPlane responseFromPlane;
        var commandType = new TypeToken<ResponseFromPlane>(){}.getType();
        responseFromPlane=gson.fromJson(rawCommand,commandType);
        return responseFromPlane;
    }
}
