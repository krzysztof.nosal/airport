package json;

public enum CommandType {
    POSITION_REQUEST,
    GO_TO_POINT,
    REACH_CIRCLE_AND_GO_AROUND,
    LAND,
    GO_TO_ANOTHER_AIRPORT,
    IDENTIFY
}
