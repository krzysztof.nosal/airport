package json;

public enum ResponseMessage {
    REACHED_DESTINATION,
    CIRCLING,
    LANDED,
    ERROR,
    IDENTIFICATION
}
