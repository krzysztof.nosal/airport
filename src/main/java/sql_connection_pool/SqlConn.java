package sql_connection_pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.concurrent.atomic.AtomicBoolean;

public class SqlConn implements SqlConInterface {
    private static final String DBURL = "jdbc:mysql://localhost:3306/airtraffic";
    private static final String DBUSER = "root";
    private static final String DBPASS = "";
    private final AtomicBoolean isFree;
    private Connection con;
    private Statement sqlStatement = null;

    public SqlConn() {
        isFree = new AtomicBoolean(true);
        this.con = createConnection(DBURL, DBUSER, DBPASS);
        createStatement();

    }

    @Override
    public boolean getIsFree() {
        return isFree.get();
    }

    private Connection createConnection(String url, String user, String Pass) {
        try {
            return DriverManager.getConnection(url, user, Pass);
        } catch (Exception e) {
            return null;
        }
    }

    private void createStatement() {
        while (this.sqlStatement == null) {
            try {
                this.sqlStatement = this.con.createStatement();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public Connection getCon() {
        isFree.set(false);
        return con;
    }

    @Override
    public void executeUpdate(String queryString) {
        try {
            sqlStatement.executeUpdate(queryString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public ResultSet executeQuery(String queryString) {
        try {
           return sqlStatement.executeQuery(queryString);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void setFree() {
        isFree.set(true);
    }
}
