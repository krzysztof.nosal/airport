package sql_connection_pool;

import java.util.concurrent.atomic.AtomicBoolean;

public class PoolCleaner extends Thread{
    private final ConnectionPool pool;
    private final AtomicBoolean running;
    public PoolCleaner(ConnectionPool pool) {
        this.pool = pool;
        this.running=new AtomicBoolean(true);
    }

    public void terminate(){
        running.set(false);
    }

    @Override
    public void run() {
        while (running.get()){
            pool.decreasePool();
            try{
            Thread.sleep(5000);}catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
