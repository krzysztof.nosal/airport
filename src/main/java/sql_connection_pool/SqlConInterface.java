package sql_connection_pool;

import java.sql.ResultSet;

public interface SqlConInterface {
    boolean getIsFree();

    void executeUpdate(String queryString);

    ResultSet executeQuery(String queryString);

    void setFree();
}
