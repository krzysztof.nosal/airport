package sql_connection_pool;

import java.sql.Connection;
import java.util.Vector;

public class ConnectionPool extends Thread{
    Vector<SqlConn>pool;

    public ConnectionPool() {
        this.pool = new Vector<>();
        for (var i=0;i<10;i++){
            this.pool.add(getConnection());
        }
    }

    private SqlConn getConnection(){
        return new SqlConn();
    }
    private void increasePool(){
        var connection=new SqlConn();
        try{
            Thread.sleep(1000);
        }catch (Exception e){
            e.printStackTrace();
        }
        this.pool.add(connection);
    }

    public void decreasePool(){
        if(pool.size()>10&&getFreeConnectionsCount()>10){
            for (var i=0;i<pool.size();i++){
                if(pool.get(i).getIsFree()&&i<pool.size()-2){
                    pool.remove(i);
                    break;
                }
            }
        }
    }

    public int getPoolSize(){
        return pool.size();
    }

    public SqlConn getSqlCon(){
        for(SqlConn con:pool){
            if(con.getIsFree()){
                con.getCon();
                return con;
            }
        }

        var poolSize = getPoolSize();
        if(poolSize<200){
        increasePool();
        while (poolSize==getPoolSize()){
            ;
        }
            return getSqlCon();
        }
       return null;
    }

    public int getFreeConnectionsCount(){
        var count=0;
        for(SqlConn sqlConn:pool){
            if(sqlConn.getIsFree()){
                count++;
            }
        }
        return count;

    }
    public int getActiveConnectionsCount(){
        var count=0;
        for(SqlConn sqlConn:pool){
            if(!sqlConn.getIsFree()){
                count++;
            }
        }
        return count;

    }

    @Override
    public void run() {
        if(getFreeConnectionsCount()<3){
            for(var i=0;i<3;i++){
                increasePool();
            }
        }
        try {
            Thread.sleep(5000);
        }catch (Exception e){}
    }
}
