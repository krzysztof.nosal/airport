package air_traffic_control;

import common.Position;

import sql_connection_pool.SqlConInterface;



import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

public class Radar extends Thread {
    private final Map<Integer, Position> positionMap;
    private final SqlConInterface sqlConnection;
    private static final int TIME_INTERVAL = 2000;
    private final AtomicLong timestamp;
    private final AtomicBoolean running;

    public Radar(SqlConInterface connection, AtomicLong timestamp) {
        this.timestamp = timestamp;
        this.positionMap = new ConcurrentHashMap<>();
        this.running = new AtomicBoolean(true);
        this.sqlConnection = connection;
    }

    public Position getPosition(Integer planeNo) {
        return positionMap.get(planeNo);
    }

    @Override
    public void run() {
        try  {
            while (running.get()) {
                Thread.sleep(TIME_INTERVAL);
                String query;
                if (timestamp.get() < 100) {
                    query = "SELECT plane_no, x_position, y_position, z_position FROM plane_monitoring WHERE flag < 99 AND timestamp>0;";
                } else {
                    query = "SELECT plane_no, x_position, y_position, z_position FROM plane_monitoring WHERE flag < 99 AND timestamp>" + (timestamp.get() - 99) + " ORDER BY timestamp ASC;";
                }
                var resultSet = sqlConnection.executeQuery(query);
                while (resultSet.next()) {
                    if (positionMap.replace(resultSet.getInt("plane_no"), new Position(resultSet.getFloat("x_position"), resultSet.getFloat("y_position"), resultSet.getFloat("z_position"))) == null) {
                        positionMap.put(resultSet.getInt("plane_no"), new Position(resultSet.getFloat("x_position"), resultSet.getFloat("y_position"), resultSet.getFloat("z_position")));
                    }
                }
                Thread.sleep(TIME_INTERVAL);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

