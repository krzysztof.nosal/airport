package air_traffic_control;

import common.Position;
import json.CommandFromAirTraffic;
import json.CommandType;
import json.JsonCode;

public class PlaneManager {

    private final CommandFromAirTraffic commandToSend;
    private final AirMap airMap;
    private final Position initialPosition;
    private final int planeNo;
    private final PlaneManagerConnection planeManagerConnection;
    private int planeFlag;

    public PlaneManager(PlaneManagerConnection planeManagerConnection, int planeNo, Position initialPosition, AirMap airMap) {
        this.planeManagerConnection = planeManagerConnection;
        this.planeNo = planeNo;
        this.planeFlag = 0;
        this.initialPosition = initialPosition;
        this.airMap = airMap;
        this.commandToSend = new CommandFromAirTraffic(new Position(0, 0, 0));

    }

    public int getPlaneFlag() {
        return planeFlag;
    }

    public void setPlaneFlag(int planeFlag) {
        this.planeFlag = planeFlag;
    }

    public Position getInitialPosition() {
        return initialPosition;
    }

    public void sendToCircle(int level, Integer height) {
        commandToSend.setCommandType(CommandType.REACH_CIRCLE_AND_GO_AROUND);
        commandToSend.setCirclingRadius(airMap.getLandingStage(level).getListOfAvailableCircles().get(height).getRadius());
        commandToSend.setCirclingCenter(airMap.getLandingStage(level).getListOfAvailableCircles().get(height).getCenter());
        planeManagerConnection.sendMessageToPlane(JsonCode.codingCommandFromAirTraffic(commandToSend));

    }

    public void sendToRunway(int runwayNo) {
        commandToSend.setCommandType(CommandType.LAND);
        commandToSend.setRunwayDefinition(airMap.getListOfRunways().get(runwayNo).getRunwayDefinition());
        planeManagerConnection.sendMessageToPlane(JsonCode.codingCommandFromAirTraffic(commandToSend));

    }

    @Override
    public String toString() {
        return "planeNo:" + planeNo + "  plane flag: " + planeFlag;
    }
}
