package air_traffic_control;

import common.Position;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicLong;

public class LandingStage {
    private final int minHeightOfCircles;
    private final int maxHeightOfCircles;
    private final int distanceBetweenCircles;
    private final LinkedHashMap<Integer, Circle> listOfAvailableCircles;
    private final Map<Integer, AirZone> blockedAirZones;
    private final Queue<PlaneInQueue> planesFlyingToThisCircle;
    private final Queue<PlaneInQueue> planesOnCircle;

    public LandingStage(int minHeightOfCircles, int maxHeightOfCircles, int radiusOfCircle, int xCircleCoordinate,
                        int yCircleCoordinate, int distanceBetweenCircles) {
        this.maxHeightOfCircles = maxHeightOfCircles;
        this.minHeightOfCircles = minHeightOfCircles;
        this.distanceBetweenCircles = distanceBetweenCircles;
        this.listOfAvailableCircles = new LinkedHashMap<>();
        for (var i = minHeightOfCircles; i <= maxHeightOfCircles; i += distanceBetweenCircles) {
            listOfAvailableCircles.put(i, new Circle(radiusOfCircle, new Position(xCircleCoordinate, yCircleCoordinate, i)));
        }
        this.blockedAirZones = new LinkedHashMap<>();
        this.planesFlyingToThisCircle = new LinkedBlockingQueue<>();
        this.planesOnCircle = new LinkedBlockingQueue<>();

    }


    public int findClosestFreeCircle(int startHeight) {
        if (startHeight < minHeightOfCircles || startHeight > maxHeightOfCircles) {
            return 0;
        }
        var initialCircleHeight = startHeight / distanceBetweenCircles * distanceBetweenCircles;
        var circleHeight = initialCircleHeight;
        var multiplier = 1;
        while (listOfAvailableCircles.get(circleHeight) != null && !listOfAvailableCircles.get(circleHeight).isFree()) {
            circleHeight -= distanceBetweenCircles * multiplier;
            if (circleHeight <= minHeightOfCircles || isCrossingBlockedAirZone(startHeight, circleHeight)) {
                circleHeight = initialCircleHeight;
                multiplier = -1;
            }
            if (circleHeight >= maxHeightOfCircles || isCrossingBlockedAirZone(startHeight, circleHeight)) {
                circleHeight = 0;
                break;
            }
        }
        return circleHeight;

    }

    private void reserveFlightZone(int planeNo, int targetHeight, int startHeight) {
        int startHeightProcessed = startHeight;
        if (targetHeight == startHeightProcessed) {
            startHeightProcessed -= 50;
        }
        blockedAirZones.put(planeNo, new AirZone(targetHeight, startHeightProcessed));
        listOfAvailableCircles.get(targetHeight).setReservation(true);
    }

    private boolean isCrossingBlockedAirZone(int startAlt, int endAlt) {
        Set<Integer> hashSet = blockedAirZones.keySet();
        if (startAlt > endAlt) {

            for (Integer planeNo : hashSet) {
                if (startAlt > blockedAirZones.get(planeNo).getMinHeight() && endAlt < blockedAirZones.get(planeNo).getMaxHeight()) {
                    return true;
                }
            }
        } else {
            for (Integer planeNo : hashSet) {
                if (startAlt < blockedAirZones.get(planeNo).getMaxHeight() && endAlt > blockedAirZones.get(planeNo).getMinHeight()) {
                    return true;
                }
            }

        }
        return false;
    }

    public void sendPlaneToCircle(PlaneInQueue plane, int startHeight) {
        planesFlyingToThisCircle.add(plane);
        reserveFlightZone(plane.planeNo, plane.targetHeight, startHeight);
        listOfAvailableCircles.get(plane.targetHeight).setReservation(true);
    }

    public boolean checkIfPlaneOnCircleAnProceed(AtomicLong timestamp, int timeBetweenCircles) {
        if (timestamp.get() - getFirstPlaneFromIntermediateStage().timestamp > timeBetweenCircles) {
            listOfAvailableCircles.get(getFirstPlaneFromIntermediateStage().targetHeight).setReservation(false);
            listOfAvailableCircles.get(getFirstPlaneFromIntermediateStage().targetHeight).addPlane(getFirstPlaneFromIntermediateStage().planeNo);
            blockedAirZones.remove(getFirstPlaneFromIntermediateStage().planeNo);
            planesOnCircle.add(getFirstPlaneFromIntermediateStage());
            deleteFirstPlaneFromIntermediateStage();
            return true;
        } else {
            return false;
        }
    }

    public boolean arePlanesOnLandingStage() {
        return !planesOnCircle.isEmpty();
    }

    public void removePlaneFromStage(PlaneInQueue processedPlane) {
        listOfAvailableCircles.get(processedPlane.targetHeight).removePlane(processedPlane.planeNo);
        deleteFirstPlaneFromLandingStage();
    }

    //getters
    public Map<Integer, AirZone> getBlockedAirZones() {
        return blockedAirZones;
    }

    public Map<Integer, Circle> getListOfAvailableCircles() {
        return listOfAvailableCircles;
    }

    public PlaneInQueue getFirstPlaneFromLandingStage() {
        return planesOnCircle.peek();
    }

    public PlaneInQueue getFirstPlaneFromIntermediateStage() {
        return planesFlyingToThisCircle.peek();
    }

    //removing planes from queues
    public void deleteFirstPlaneFromLandingStage() {
        planesOnCircle.poll();
    }

    private void deleteFirstPlaneFromIntermediateStage() {
        planesFlyingToThisCircle.poll();
    }
}
