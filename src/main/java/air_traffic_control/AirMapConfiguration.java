package air_traffic_control;

public class AirMapConfiguration {
    private final int numberOfCircles ;
    private final int minHeightOfCircles ;
    private final int maxHeightOfCircles ;
    private final int distanceBetweenCircles ;
    private  final int biggestCircleRadius ;
    private  final int horizontalDistanceBetweenCircles ;
    private  final int xCenterCoordinate ;
    private  final int yCenterCoordinate ;

    public AirMapConfiguration(int numberOfCircles, int minHeightOfCircles, int maxHeightOfCircles, int distanceBetweenCircles, int biggestCircleRadius, int horizontalDistanceBetweenCircles, int xCenterCoordinate, int yCenterCoordinate) {
        this.numberOfCircles = numberOfCircles;
        this.minHeightOfCircles = minHeightOfCircles;
        this.maxHeightOfCircles = maxHeightOfCircles;
        this.distanceBetweenCircles = distanceBetweenCircles;
        this.biggestCircleRadius = biggestCircleRadius;
        this.horizontalDistanceBetweenCircles = horizontalDistanceBetweenCircles;
        this.xCenterCoordinate = xCenterCoordinate;
        this.yCenterCoordinate = yCenterCoordinate;
    }

    public int getNumberOfCircles() {
        return numberOfCircles;
    }

    public int getMinHeightOfCircles() {
        return minHeightOfCircles;
    }

    public int getMaxHeightOfCircles() {
        return maxHeightOfCircles;
    }

    public int getDistanceBetweenCircles() {
        return distanceBetweenCircles;
    }

    public int getBiggestCircleRadius() {
        return biggestCircleRadius;
    }

    public int getHorizontalDistanceBetweenCircles() {
        return horizontalDistanceBetweenCircles;
    }

    public int getxCenterCoordinate() {
        return xCenterCoordinate;
    }

    public int getyCenterCoordinate() {
        return yCenterCoordinate;
    }
}
