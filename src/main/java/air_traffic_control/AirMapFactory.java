package air_traffic_control;

import common.Position;
import json.Runway;

import java.util.LinkedList;
import java.util.List;


public class AirMapFactory {
    private AirMapFactory() {
    }

    public static AirMap prepareDefaultAirmap(Radar radar) {
        var airMapConfiguration = new AirMapConfiguration(3, 2000, 10000, 500, 4000, 500, 5000, 5000);
        List<RunwayMonitoring> listOfRunways = new LinkedList<>();

        List<Position> runwayDefinition = new LinkedList<>();
        runwayDefinition.add(new Position(4500, 4500, 1000));
        runwayDefinition.add(new Position(4500, 4650, 790));
        runwayDefinition.add(new Position(4500, 4800, 605));
        runwayDefinition.add(new Position(4500, 4950, 445));
        runwayDefinition.add(new Position(4500, 5100, 310));
        runwayDefinition.add(new Position(4500, 5250, 200));
        runwayDefinition.add(new Position(4500, 5400, 115));
        runwayDefinition.add(new Position(4500, 5550, 55));
        runwayDefinition.add(new Position(4500, 5700, 20));
        runwayDefinition.add(new Position(4500, 5850, 10));
        runwayDefinition.add(new Position(4500, 6000, 0));


        listOfRunways.add(new RunwayMonitoring(new Runway(runwayDefinition)));
        runwayDefinition = new LinkedList<>();
        runwayDefinition.add(new Position(5500, 5500, 1000));
        runwayDefinition.add(new Position(5500, 5350, 790));
        runwayDefinition.add(new Position(5500, 5200, 605));
        runwayDefinition.add(new Position(5500, 5050, 445));
        runwayDefinition.add(new Position(5500, 4900, 310));
        runwayDefinition.add(new Position(5500, 4750, 200));
        runwayDefinition.add(new Position(5500, 4600, 115));
        runwayDefinition.add(new Position(5500, 4450, 55));
        runwayDefinition.add(new Position(5500, 4300, 20));
        runwayDefinition.add(new Position(5500, 4150, 10));
        runwayDefinition.add(new Position(5500, 4000, 0));

        listOfRunways.add(new RunwayMonitoring(new Runway(runwayDefinition)));
        return new AirMap(radar, airMapConfiguration, listOfRunways);

    }

}
