package air_traffic_control;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class PlaneManagerConnection {
    private DataInputStream inputStream;
    private DataOutputStream outputStream;

    public PlaneManagerConnection(Socket socket) {
        try {
            this.inputStream = new DataInputStream(socket.getInputStream());
            this.outputStream = new DataOutputStream(socket.getOutputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void sendMessageToPlane(String message) {
        try {
            outputStream.writeUTF(message);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
