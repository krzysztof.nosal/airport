package air_traffic_control;

import json.Runway;

public class RunwayMonitoring {
    private final Runway runwayDefinition;
    private int planeNo;
    private boolean occupied;

    public RunwayMonitoring(Runway runwayDefinition) {
        this.runwayDefinition = runwayDefinition;
        this.planeNo = 0;
        this.occupied = false;
    }

    public Runway getRunwayDefinition() {
        return runwayDefinition;
    }

    public int getPlaneNo() {
        return planeNo;
    }

    public boolean isOccupied() {
        return occupied;
    }

    public void setFree(){
        occupied=false;
        planeNo=0;
    }
    public void sendPlaneToRunway(int planeNo){
        occupied=true;
        this.planeNo = planeNo;
    }
}
