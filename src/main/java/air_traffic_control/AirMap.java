package air_traffic_control;

import java.util.*;

public class AirMap {
    private final List<RunwayMonitoring> listOfRunways;

    private final Radar radar;

    private  final List<LandingStage>landingStages;

    public AirMap( Radar radar, AirMapConfiguration airMapConfiguration, List<RunwayMonitoring> listOfRunways) {
        this.listOfRunways = listOfRunways;
        this.radar = radar;
        landingStages = new LinkedList<>();
        for (var i = 0; i < airMapConfiguration.getNumberOfCircles(); i++) {
            landingStages.add(new LandingStage(airMapConfiguration.getMinHeightOfCircles(),airMapConfiguration.getMaxHeightOfCircles(),
                    airMapConfiguration.getBiggestCircleRadius() - (i * airMapConfiguration.getHorizontalDistanceBetweenCircles()),
                    airMapConfiguration.getxCenterCoordinate(),
                    airMapConfiguration.getyCenterCoordinate(),
                    airMapConfiguration.getDistanceBetweenCircles()));
        }
            }

    public List<RunwayMonitoring> getListOfRunways() {
        return listOfRunways;
    }

    public List<LandingStage> getLandingStages() {
        return landingStages;
    }

    public LandingStage getLandingStage(int level){
        return landingStages.get(level);
    }
    public boolean arePlanesOnLastLandingStage() {
        return landingStages.get(landingStages.size()-1).arePlanesOnLandingStage();
    }

    public boolean canLand() {
        var cantLand = 0;
        for (var j = 0; j < getListOfRunways().size(); j++) {
            if (getListOfRunways().get(j).isOccupied() && radar.getPosition(getListOfRunways().get(j).getPlaneNo()).getZ() > 1200) {
                cantLand++;
            }
        }
        return cantLand < (getListOfRunways().size() - 1);
    }


    @Override
    public String toString() {
        var stringBuilder = new StringBuilder();
        stringBuilder.append("Airmap: ");
        stringBuilder.append("number of circles: ").append(landingStages.size());

        return stringBuilder.toString();
    }
}
