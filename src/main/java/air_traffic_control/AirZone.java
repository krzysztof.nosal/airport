package air_traffic_control;

public class AirZone {
    int minHeight;
    int maxHeight;

    public AirZone(int height1, int height2) {
        if(height1>=height2){
            this.minHeight = height2;
            this.maxHeight = height1;
        }else
        {
            this.minHeight = height1;
            this.maxHeight = height2;
        }

    }

    public int getMinHeight() {
        return minHeight;
    }

    public int getMaxHeight() {
        return maxHeight;
    }
}
