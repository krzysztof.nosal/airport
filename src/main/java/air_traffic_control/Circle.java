package air_traffic_control;

import common.Position;

import java.util.LinkedList;
import java.util.List;

public class Circle {
    private final List<Integer> planes;
    private final int radius;
    private final Position center;
    private boolean reservation;

    public Circle(int radius, Position center) {
        this.radius = radius;
        this.center = center;
        planes = new LinkedList<>();
    }

    public int getRadius() {
        return radius;
    }

    public Position getCenter() {
        return center;
    }

    public boolean isReserved() {
        return reservation;
    }

    public void setReservation(boolean reservation) {
        this.reservation = reservation;
    }

    public int getPlanesCount() {
        return planes.size();
    }

    public void addPlane(int planeNo) {
        if (planes.size() < 3) {
            planes.add(planeNo);
        }
    }

    public void removePlane(int planeNo) {
        for (var i = 0; i < planes.size(); i++) {
            if (planes.get(i) == planeNo) {
                planes.remove(i);
                break;
            }
        }
    }

    public boolean isFree() {
        return !isReserved() && getPlanesCount() < 2;
    }

}
