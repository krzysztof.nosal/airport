package air_traffic_control;

public class PlaneInQueue {
    Integer planeNo;
    long timestamp;
    int targetHeight=0;

    public int getTargetHeight() {
        return targetHeight;
    }

    public void setTargetHeight(int targetHeight) {
        this.targetHeight = targetHeight;
    }

    public PlaneInQueue(Integer planeNo, long timestamp) {
        this.planeNo = planeNo;
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return planeNo.toString();
    }
}
