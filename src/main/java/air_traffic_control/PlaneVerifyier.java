package air_traffic_control;

import common.Position;
import json.CommandFromAirTraffic;
import json.CommandType;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.concurrent.ConcurrentMap;

public class PlaneVerifyier extends Thread {
    private final ConcurrentMap<Integer, PlaneManager> activeConnections;
    private final Socket s;
    private final AirMap airMap;

    public PlaneVerifyier(ConcurrentMap<Integer, PlaneManager> activeConnections, Socket s, AirMap airMap) {
        this.airMap=airMap;
        this.activeConnections = activeConnections;
        this.s = s;

    }

    private void sendToAnotherAirport(DataOutputStream tempDataOutStream) {
        try {
            var commandFromAirTraffic = new CommandFromAirTraffic(new Position(0, 0, 0));
            commandFromAirTraffic.setCommandType(CommandType.GO_TO_ANOTHER_AIRPORT);
            tempDataOutStream.writeUTF(json.JsonCode.codingCommandFromAirTraffic(commandFromAirTraffic));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void run() {
        try {
            var tempDataOutStream = new DataOutputStream(s.getOutputStream());
            var tempDataInStream = new DataInputStream(s.getInputStream());
            if (activeConnections.size() < 100) {
                String receivedMessage = null;
                var commandFromAirTraffic = new CommandFromAirTraffic(new Position(0, 0, 0));
                commandFromAirTraffic.setCommandType(CommandType.IDENTIFY);

                tempDataOutStream.writeUTF(json.JsonCode.codingCommandFromAirTraffic(commandFromAirTraffic));
                while (tempDataInStream.available() == 0) {
                    tempDataOutStream.writeUTF(json.JsonCode.codingCommandFromAirTraffic(commandFromAirTraffic));

                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                receivedMessage = tempDataInStream.readUTF();
                var responseFromPlane = json.JsonDecode.responseFromPlane(receivedMessage);

                if (responseFromPlane != null) {
                    var planeManager = new PlaneManager(new PlaneManagerConnection(s), responseFromPlane.getPlaneNo(), responseFromPlane.getPosition(),airMap);
                    activeConnections.put(responseFromPlane.getPlaneNo(), planeManager);
                } else {
                    sendToAnotherAirport(tempDataOutStream);
                    s.close();

                }
            } else {
                sendToAnotherAirport(tempDataOutStream);
                s.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
