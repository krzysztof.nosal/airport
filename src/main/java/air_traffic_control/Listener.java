package air_traffic_control;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;

public class Listener extends Thread {
    private final ConcurrentMap<Integer,PlaneManager> activeConnections;
    private final AtomicBoolean running;
    private final int port;
    private final AirMap airMap;



    public Listener(ConcurrentMap<Integer,PlaneManager> activeConnections, int port, AirMap airMap) {
        this.airMap = airMap;
        this.activeConnections = activeConnections;
        this.port = port;
        this.running = new AtomicBoolean(true);
    }

    public void abort() {
        running.set(false);
    }

    @Override
    public void run() {
        try(var ss = new ServerSocket(port)) {
            while (running.get()) {
                Socket s = ss.accept();
                var planeVerifier=  new PlaneVerifyier(this.activeConnections, s, airMap);
                planeVerifier.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
