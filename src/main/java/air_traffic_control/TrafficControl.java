package air_traffic_control;


import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

public class TrafficControl extends Thread {
    private final AirMap airMap;
    private final ConcurrentMap<Integer, PlaneManager> planeManagerConcurrentMap;
    private final Radar radar;
    private final AtomicLong timestamp;
    private final AtomicBoolean running;

    public TrafficControl(ConcurrentMap<Integer, PlaneManager> planeManagerConcurrentMap, Radar radar, AtomicLong timestamp, AirMap airmap) {
        this.planeManagerConcurrentMap = planeManagerConcurrentMap;
        this.radar = radar;
        this.airMap = airmap;
        this.timestamp = timestamp;
        this.running= new AtomicBoolean(true);
    }

    private Integer findNextPlaneWithSpecifiedFlag(int flag) {
        Set<Integer> planeNumbers = planeManagerConcurrentMap.keySet();
        for (Integer planeNo : planeNumbers) {
            if (planeManagerConcurrentMap.get(planeNo).getPlaneFlag() == flag) {
                return planeNo;
            }
        }
        return null;
    }

    private void assignPlaneToCircles() {
        for (var i = 0; i < airMap.getLandingStages().size(); i ++) {
            LandingStage landingStage = airMap.getLandingStage(i);
            var processedPlane = landingStage.getFirstPlaneFromIntermediateStage();
            if (landingStage.checkIfPlaneOnCircleAnProceed(timestamp,30)){
                planeManagerConcurrentMap.get(processedPlane.planeNo).setPlaneFlag(2);
            }
        }
    }

    private void sendAllNewPlanesToFirstCircle() {
        if (!planeManagerConcurrentMap.isEmpty()) {
            while (findNextPlaneWithSpecifiedFlag(88) != null) {
                var foundPlane = findNextPlaneWithSpecifiedFlag(88);
                planeManagerConcurrentMap.get(foundPlane).setPlaneFlag(0);
            }
            while (findNextPlaneWithSpecifiedFlag(0) != null) {
                var foundPlane = findNextPlaneWithSpecifiedFlag(0);
                int startHeight = (int) planeManagerConcurrentMap.get(foundPlane).getInitialPosition().getZ();
                int circleHeight = airMap.getLandingStage(0).findClosestFreeCircle(startHeight);
                if (foundPlane != null && circleHeight > 0) {
                    planeManagerConcurrentMap.get(foundPlane).setPlaneFlag(1);
                    planeManagerConcurrentMap.get(foundPlane).sendToCircle(0, circleHeight);
                    var planeInQueue = new PlaneInQueue(foundPlane, timestamp.get());
                    planeInQueue.setTargetHeight(circleHeight);
                    airMap.getLandingStage(0).sendPlaneToCircle(planeInQueue,startHeight);
                } else {
                    planeManagerConcurrentMap.get(foundPlane).setPlaneFlag(88);
                }
            }
        }
    }

    private void sendPlanesBetweenCircles() {
        for (var i = 1; i < (airMap.getLandingStages().size() - 1); i ++) {
            while (airMap.getLandingStage(i).arePlanesOnLandingStage()) {
                LandingStage landingStage = airMap.getLandingStage(i);
                PlaneInQueue processedPlane = landingStage.getFirstPlaneFromLandingStage();
                Integer foundPlane = processedPlane.planeNo;
                int startHeight = processedPlane.getTargetHeight();
                int circleHeight = airMap.getLandingStage(i+1).findClosestFreeCircle(startHeight);
                if (circleHeight > 0) {
                    try {
                        var successfullyProcessedPlane = new PlaneInQueue(processedPlane.planeNo,timestamp.get());
                        successfullyProcessedPlane.setTargetHeight(circleHeight);
                        airMap.getLandingStage(i+1).sendPlaneToCircle(successfullyProcessedPlane,startHeight);
                    } catch (Exception e) {
                        System.out.println("wrong circle height: " + circleHeight);
                        e.printStackTrace();
                    }
                    planeManagerConcurrentMap.get(foundPlane).setPlaneFlag(i + 2);
                    planeManagerConcurrentMap.get(foundPlane).sendToCircle((i + 1) / 2, circleHeight);
                    landingStage.deleteFirstPlaneFromLandingStage();
                } else {
                    break;
                }
            }
        }
    }

    private void sendPlanesToRunway() {
        if (airMap.arePlanesOnLastLandingStage()) {
            var lastLandingStage = airMap.getLandingStage(airMap.getLandingStages().size()-1);

            var processedPlane=lastLandingStage.getFirstPlaneFromLandingStage();
            var processedPlaneNumber = processedPlane.planeNo;

            for (var i = 0; i < airMap.getListOfRunways().size(); i++) {
                if (!airMap.getListOfRunways().get(i).isOccupied()) {
                    if (airMap.canLand()) {
                        airMap.getListOfRunways().get(i).sendPlaneToRunway(processedPlaneNumber);
                        lastLandingStage.removePlaneFromStage(processedPlane);
                        planeManagerConcurrentMap.get(processedPlaneNumber).setPlaneFlag(5);
                        planeManagerConcurrentMap.get(processedPlaneNumber).sendToRunway(i);
                    }
                    break;
                }
            }
        }
    }

    private void checkIfPlanesLanded() {
        for (var i = 0; i <= 1; i++) {
            if (airMap.getListOfRunways().get(i).isOccupied()) {
                Integer planeNo = airMap.getListOfRunways().get(i).getPlaneNo();
                if (radar.getPosition(airMap.getListOfRunways().get(i).getPlaneNo()) != null && radar.getPosition(airMap.getListOfRunways().get(i).getPlaneNo()).getZ() < 1) {
                    airMap.getListOfRunways().get(i).setFree();
                    System.out.println("plane " + planeNo + " landed.");

                    planeManagerConcurrentMap.get(planeNo).setPlaneFlag(100);
                    planeManagerConcurrentMap.remove(planeNo);
                    System.out.println("planes in air: " + planeManagerConcurrentMap.size());
                }
            }

        }
    }

    public void terminate(){
        running.set(false);
    }

    @Override
    public void run() {

        while (running.get()) {
            sendAllNewPlanesToFirstCircle();
            assignPlaneToCircles();
            sendPlanesBetweenCircles();
            sendPlanesToRunway();
            checkIfPlanesLanded();
        }
    }
}