package plane;

import common.Position;

public class PlaneConfiguration {
    private final int planeNo;
    private int speed;
    private final Position startPosition;

    public PlaneConfiguration(int planeNo, int speed, Position startPosition) {
        this.planeNo = planeNo;
        this.speed = speed;
        this.startPosition = startPosition;
    }

    public int getPlaneNo() {
        return planeNo;
    }

    public int getSpeed() {
        return speed;
    }

    public Position getStartPosition() {
        return startPosition;
    }

    public void setSpeed(int speed){
        this.speed = speed;
    }
}
