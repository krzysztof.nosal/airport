package plane;

import java.sql.Statement;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicBoolean;

public class BatchPlaneEvaluation extends Thread {
    private final Vector<Plane> planesVector;
    private final Statement batchStatement;
    private final AtomicBoolean isRunning;

    public BatchPlaneEvaluation(Vector<Plane> planesVector, Statement batchStatement) {
        this.batchStatement = batchStatement;
        this.planesVector = planesVector;
        this.isRunning=new AtomicBoolean(true);
    }

    public void abort(){
        isRunning.set(false);
    }
    @Override
    public void run() {
        while (isRunning.get()) {
            for (Plane plane : planesVector) {
                plane.evaluatePosition();
            }
            try {
                batchStatement.executeBatch();
                batchStatement.clearBatch();
            } catch (Exception e) {
                e.printStackTrace();
            }


            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
