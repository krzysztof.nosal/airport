package plane;

import java.io.DataInputStream;
import java.io.DataOutputStream;

public interface ClientConnectionHandlerInterface {
    boolean connect(int portNumber, String ipAddress);

    DataInputStream getInputStream();

    DataOutputStream getOutputStream();
}
