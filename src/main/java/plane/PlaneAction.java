package plane;

public enum PlaneAction {
    WAIT,
    GO_TO_POINT,
    CALCULATE_SHORTEST_WAY_TO_CIRCLE,
    CALCULATE_NEXT_POINT_ON_CIRCLE,
    LAND
}
