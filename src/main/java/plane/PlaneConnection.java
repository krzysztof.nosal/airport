package plane;

import common.TcpTools;
import json.CommandFromAirTraffic;

import java.io.DataOutputStream;
import java.util.concurrent.ArrayBlockingQueue;

public class PlaneConnection implements PlaneConnectionInterface {
    private final ArrayBlockingQueue<CommandFromAirTraffic> trafficControlInputBuffer;
    private final TcpClientListener tcpClientListener;
    private final DataOutputStream outputStream;

    public PlaneConnection(int tcpPortOfAirTraffic, String serverIpAddress) {
        var connectionHandler = TcpTools.getTcpConnectionHandler(tcpPortOfAirTraffic, serverIpAddress);
        outputStream = connectionHandler.getOutputStream();
        var inputStream = connectionHandler.getInputStream();
        trafficControlInputBuffer = new ArrayBlockingQueue<>(100, true);
        tcpClientListener = new TcpClientListener(trafficControlInputBuffer, inputStream);
        tcpClientListener.start();
    }

    @Override
    public ArrayBlockingQueue<CommandFromAirTraffic> getRecentCommands() {
        if (trafficControlInputBuffer.peek() != null) {
            return trafficControlInputBuffer;
        } else {
            return null;
        }
    }

    @Override
    public CommandFromAirTraffic getLastCommand() {
        return trafficControlInputBuffer.poll();
    }

    @Override
    public boolean areCommandsInBuffer() {
        return trafficControlInputBuffer.peek() != null;
    }

    @Override
    public void closeConnection() {
        tcpClientListener.close();
    }

    @Override
    public void sendMessage(String message) {
        try {
            outputStream.writeUTF(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
