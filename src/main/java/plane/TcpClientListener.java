package plane;

import json.CommandFromAirTraffic;
import json.JsonDecode;

import java.io.DataInputStream;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class TcpClientListener extends Thread {
    ArrayBlockingQueue<CommandFromAirTraffic> trafficControlInputBuffer;
    DataInputStream dataInputStream;
    AtomicBoolean close;

    public TcpClientListener(ArrayBlockingQueue<CommandFromAirTraffic> trafficControlInputBuffer, DataInputStream dataInputStream) {
        this.trafficControlInputBuffer = trafficControlInputBuffer;
        this.dataInputStream = dataInputStream;
        this.close = new AtomicBoolean(false);
    }

    public void close() {
        close.set(true);
    }

    @Override
    public void run() {
        while (!close.get()) {
            try {
                String received = dataInputStream.readUTF();
                if (JsonDecode.commandFromAirTraffic(received) != null) {
                    trafficControlInputBuffer.add(JsonDecode.commandFromAirTraffic(received));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
