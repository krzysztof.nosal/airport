package plane;

import common.Position;
import common.SqlTools;
import json.CommandFromAirTraffic;
import json.ResponseFromPlane;
import json.ResponseMessage;
import monitoring.PlanePathFileGenerator;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import static plane.PlaneAction.LAND;
import static plane.PlaneAction.WAIT;


public class Plane extends Thread {
    private static final int TIME_INTERVAL = 1000;
    private final AtomicLong timestamp;
    private final AtomicBoolean running;
    private final PlanePathFileGenerator planePathFileGenerator;
    private final PlaneConfiguration planeConfiguration;
    private final PlaneSqlConnection planeSqlConnection;
    private final PlaneConnection planeConnection;
    private Position position;
    private Position targetPosition;
    private PlaneFlag planeFlag = PlaneFlag.WAITING_OUTSIDE_AIRZONE;
    private PlaneAction currentAction = WAIT;
    private CommandFromAirTraffic currentlyExecutedCommand;

    public Plane(PlaneConfiguration planeConfiguration, PlaneSqlConnection planeSqlConnection, AtomicLong timestamp, PlaneConnection planeConnection, PlanePathFileGenerator planePathFileGenerator) {
        this.planeConnection = planeConnection;
        this.planePathFileGenerator = planePathFileGenerator;
        this.planeConfiguration = planeConfiguration;
        this.timestamp = timestamp;
        this.currentlyExecutedCommand = new CommandFromAirTraffic(planeConfiguration.getStartPosition());
        this.running = new AtomicBoolean(true);
        this.currentAction = WAIT;
        this.planeSqlConnection = planeSqlConnection;
        this.position = planeConfiguration.getStartPosition();
    }

    public PlaneConfiguration getPlaneConfiguration() {
        return planeConfiguration;
    }

    public Position getPosition() {
        return position;
    }

    private void savePositionToDB() {
        String query = SqlTools.createSavePositionToDbStatement(this, timestamp);
        System.out.println("test: " + query);
        planeSqlConnection.executeQuery(query);
    }

    public int getPlaneNo() {
        return planeConfiguration.getPlaneNo();
    }

    public PlaneFlag getPlaneFlag() {
        return planeFlag;
    }

    private void checkNewCommands() {
        CommandFromAirTraffic commandFromAirTraffic = null;
        var isSthInBuffer = false;
        while (planeConnection.areCommandsInBuffer()) {
            commandFromAirTraffic = planeConnection.getLastCommand();
            isSthInBuffer = true;
            this.currentlyExecutedCommand = commandFromAirTraffic;
        }
        if (isSthInBuffer) {
            switch (this.currentlyExecutedCommand.getCommandType()) {
                case IDENTIFY:
                    planeConnection.sendMessage(json.JsonCode.codingResponseFromPlane(new ResponseFromPlane(planeConfiguration.getPlaneNo(), position, ResponseMessage.IDENTIFICATION)));
                    break;
                case GO_TO_POINT:
                    targetPosition = currentlyExecutedCommand.getTargetPosition();
                    currentAction = PlaneAction.GO_TO_POINT;
                    planeFlag = PlaneFlag.FLYING_TO_POINT;
                    break;
                case REACH_CIRCLE_AND_GO_AROUND:
                    currentAction = PlaneAction.CALCULATE_SHORTEST_WAY_TO_CIRCLE;
                    break;
                case LAND:
                    currentAction = LAND;
                    break;
                case GO_TO_ANOTHER_AIRPORT:
                    planeConnection.closeConnection();
                    planeFlag = PlaneFlag.GOING_TO_ANOTHER_AIRPORT;
                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    running.set(false);
                    break;
                default:
                    System.out.println("unidentified command from air traffic!!!!");
            }
        }
    }

    private boolean isOnPosition() {
        return Navigator.distanceBetweenPositions(position, targetPosition) < 10;
    }

    private void goToNextRunwayPosition() {
        if (currentlyExecutedCommand.getRunwayDefinition() == null) {
            currentAction = WAIT;
            return;
        }
        if (currentlyExecutedCommand.getRunwayDefinition().getPositionsArray().isEmpty()) {
            currentAction = WAIT;
            planeFlag = PlaneFlag.LANDED;
            planePathFileGenerator.getPathAndSaveToFile(planeConfiguration.getPlaneNo());
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            savePositionToDB();
            planeConnection.closeConnection();
            running.set(false);
        } else {
            targetPosition = currentlyExecutedCommand.getRunwayDefinition().getPositionsArray().get(0);
            currentlyExecutedCommand.getRunwayDefinition().getPositionsArray().remove(0);
            planeFlag = PlaneFlag.LANDING;
            currentAction = PlaneAction.GO_TO_POINT;
        }
    }

    public void evaluatePosition() {

        checkNewCommands();

        switch (currentAction) {
            case CALCULATE_SHORTEST_WAY_TO_CIRCLE -> {
                targetPosition = Navigator.findClosestPointOnCircle(currentlyExecutedCommand.getCirclingCenter(), position, currentlyExecutedCommand.getCirclingRadius());
                currentAction = PlaneAction.GO_TO_POINT;
            }
            case CALCULATE_NEXT_POINT_ON_CIRCLE -> {
                targetPosition = Navigator.findNextPositionOnCircle(position, currentlyExecutedCommand.getCirclingCenter(), planeConfiguration.getSpeed(), TIME_INTERVAL / 1000);
                currentAction = PlaneAction.GO_TO_POINT;
            }
            case LAND -> goToNextRunwayPosition();
        }

        if (currentAction == PlaneAction.GO_TO_POINT) {
            if (!isOnPosition()) {
                position = Navigator.flyStraightToTarget(position, targetPosition, planeConfiguration.getSpeed(), TIME_INTERVAL / 1000);
            } else {
                switch (planeFlag) {
                    case FLYING_TO_CIRCLE, WAITING_ON_CIRCLE -> {
                        planeFlag = PlaneFlag.WAITING_ON_CIRCLE;
                        currentAction = PlaneAction.CALCULATE_NEXT_POINT_ON_CIRCLE;
                    }
                    case LANDING -> currentAction = LAND;
                    default -> currentAction = WAIT;

                }
            }
        }
        savePositionToDB();

    }

    @Override
    public void run() {
        while (running.get()) {
            evaluatePosition();
            try {
                Thread.sleep(TIME_INTERVAL);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }
}
