package plane;

import common.SqlTools;
import sql_connection_pool.ConnectionPool;
import sql_connection_pool.SqlConn;

import java.sql.Statement;

public class PlaneSqlConnection {
    private Statement batchStatement = null;
  //  private Statement statement = null;
    private SqlConn sqlConn;
    private final boolean usingBatchStatementForDb;

    public PlaneSqlConnection(Statement batchStatement) {
        this.batchStatement = batchStatement;
        this.usingBatchStatementForDb = true;
    }

    public PlaneSqlConnection(ConnectionPool connectionPool) {
        this.sqlConn = SqlTools.getConnectionFromPool(connectionPool);
        this.usingBatchStatementForDb = false;
    }

    public void executeQuery(String query) {
        try {
            if (usingBatchStatementForDb) {
                batchStatement.addBatch(query);
            } else {
                sqlConn.executeUpdate(query);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
