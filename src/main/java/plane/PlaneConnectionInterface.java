package plane;

import json.CommandFromAirTraffic;

import java.util.concurrent.ArrayBlockingQueue;

public interface PlaneConnectionInterface {
    ArrayBlockingQueue<CommandFromAirTraffic> getRecentCommands();

    CommandFromAirTraffic getLastCommand();

    boolean areCommandsInBuffer();

    void closeConnection();

    void sendMessage(String message);
}
