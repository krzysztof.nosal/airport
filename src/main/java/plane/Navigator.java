package plane;

import common.Position;


public class Navigator {
    private Navigator(){}
    public static Position flyStraightToTarget(Position currentPosition, Position targetPosition, int speedKmH, int timeIncrementS) {
        if (currentPosition==null||targetPosition==null||speedKmH<1||timeIncrementS<1){
            return null;
        }
        float distanceToGo = speedKmH * 1000 * (float)timeIncrementS / 3600;
        float totalDistance = (float) Math.sqrt((float) Math.pow(targetPosition.getX() - currentPosition.getX(), 2) + Math.pow(targetPosition.getY() - currentPosition.getY(), 2) + Math.pow(targetPosition.getZ() - currentPosition.getZ(), 2));
        float xIncrement = ((targetPosition.getX() - currentPosition.getX()) * distanceToGo / totalDistance);
        float yIncrement = ((targetPosition.getY() - currentPosition.getY()) * distanceToGo / totalDistance);
        float zIncrement = ((targetPosition.getZ() - currentPosition.getZ()) * distanceToGo / totalDistance);

        var newPosition = new Position((currentPosition.getX() + xIncrement), (currentPosition.getY() + yIncrement), (currentPosition.getZ() + zIncrement));
        if (distanceBetweenPositions(currentPosition, targetPosition) < distanceBetweenPositions(currentPosition, newPosition)) {
            return targetPosition;
        } else {
            return newPosition;
        }
    }

    public static float distanceBetweenPositions(Position pos1, Position pos2) {
        return (float) Math.sqrt((float) Math.pow(pos2.getX() - pos1.getX(), 2) + Math.pow(pos2.getY() - pos1.getY(), 2) + Math.pow(pos2.getZ() - pos1.getZ(), 2));
    }

    public static Position findClosestPointOnCircle(Position center , Position currentPosition, float radius) {
        float centerY = center.getY();
        float centerX = center.getX();
        float height = center.getZ();

        float lineEquationA = (centerY - currentPosition.getY()) / (centerX - currentPosition.getX());
        float lineEquationB = currentPosition.getY() - (currentPosition.getX() * lineEquationA);

        float tempBMinusYo = lineEquationB - centerY;

        float sqEquationA = (float) Math.pow(lineEquationA, 2) + 1;
        float sqEquationB = 2 * lineEquationA * tempBMinusYo - 2 * centerX;
        float sqEquationC = (float) Math.pow(tempBMinusYo, 2) - (float) Math.pow(radius, 2) + (float) Math.pow(centerX, 2);
        float sqEquationDelta = (float) Math.pow(sqEquationB, 2) - 4 * sqEquationA * sqEquationC;

        float resultX1 = (-1 * sqEquationB + (float) Math.sqrt(sqEquationDelta)) / (2 * sqEquationA);
        float resultX2 = (-1 * sqEquationB - (float) Math.sqrt(sqEquationDelta)) / (2 * sqEquationA);
        float resultY1 = resultX1 * lineEquationA + lineEquationB;
        float resultY2 = resultX2 * lineEquationA + lineEquationB;
        var result1 = new Position(resultX1, resultY1, height);
        var result2 = new Position(resultX2, resultY2, height);
        if (distanceBetweenPositions(currentPosition, result1) < distanceBetweenPositions(currentPosition, result2)) {
            return result1;
        } else {
            return result2;
        }

    }

    public static Position findNextPositionOnCircle(Position currentPosition, Position center, float speedKmH, int timeIncrementS) {
        float radius = distanceBetweenPositions(center, currentPosition);
        float centerX = center.getX();
        float centerY = center.getY();
        float circumference = (float) (2 * Math.PI * radius);
        float distanceToGo = speedKmH * 1000 * timeIncrementS / 3600;
        int circleParts = Math.round(circumference / distanceToGo);
        float fiIncrement = (float) Math.PI * 2 / circleParts;
        float xStartRadial = currentPosition.getX() - centerX;
        float yStartRadial = currentPosition.getY() - centerY;
        float fi = (float) Math.atan2(yStartRadial, xStartRadial);
        return new Position((float) (radius * Math.cos(fi + fiIncrement))+centerX, (float) (radius * Math.sin(fi + fiIncrement))+centerY, currentPosition.getZ());
    }
}
