package plane;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

public class ClientConnectionHandler implements ClientConnectionHandlerInterface {
    Socket s = null;
    private DataInputStream inputStream = null;
    private DataOutputStream outputStream = null;

    public ClientConnectionHandler() {
        try {
            s = new Socket();
            inputStream = new DataInputStream(s.getInputStream());
            outputStream = new DataOutputStream(s.getOutputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean connect(int portNumber, String ipAddress) {

        var ip = new InetSocketAddress(ipAddress, portNumber);
        try {
            s.connect(ip);
            inputStream = new DataInputStream(s.getInputStream());
            outputStream = new DataOutputStream(s.getOutputStream());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public DataInputStream getInputStream() {
        return inputStream;
    }

    @Override
    public DataOutputStream getOutputStream() {
        return outputStream;
    }
}
